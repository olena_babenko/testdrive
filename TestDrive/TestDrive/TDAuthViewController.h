//
//  TDAuthViewController.h
//  SampleRequest
//
//  Created by Olena Babenko on 2/10/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TDAuthDelegate <NSObject>

-(void)authDidFailWithError:(NSError *)error;
-(void)authDidEndWithToken:(NSString *)token;

@end
@interface TDAuthViewController : UIViewController

@property (weak,nonatomic) id<TDAuthDelegate> delegate;

- (id)initWithClientID:(NSString *)clientID redirectPath:(NSString *)redirect delegate:(id<TDAuthDelegate>)auth_delegate;

@end
