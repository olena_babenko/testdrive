//
//  TDItemData.m
//  TestDrive
//
//  Created by Olena Babenko on 2/9/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import "TDStorageManager.h"

#import "TDRemoteStorage.h"
#import "TDLocalStorage.h"

@interface TDStorageManager ()<TDStorageResponder>
{
}

@property (weak, nonatomic) id<TDStorageResponder> responder;
@end

@implementation TDStorageManager

-(id)initWithResponder:(id)responder
{
    self = [super init];
    if (self) {
        self.responder = responder;
    }
    return self;
}
-(void)getHierarchy:(TDItem *)item
{
    [self getHierarchy:item withResponder:self];
}
-(void)getHierarchy:(TDItem *)item withResponder:(id<TDStorageResponder>)responder
{
    id<TDStorage> currentStorage;
    if (responder==self) {
        // First attempt -  is try to get it from remote server
        // Let Manager check result First But We save an responder
        currentStorage =  [[TDRemoteStorage alloc] init];
    }else
    {
        // Second attempt -  is try to get it from local database
        currentStorage = [[TDLocalStorage alloc] init];
    }
    [currentStorage getHierarchy:item withResponder:responder];

}
-(void)updateLocal:(TDItem *)item
{
    //Пока что обновить можно только локальное хранилище
    id<TDStorage> currentStorage = [[TDLocalStorage alloc] init];
    [currentStorage update:item withResponder:self.responder];
    
}
-(BOOL)isExistLocally:(TDItem *)item
{
    //Пока что проверка только локальная
    id<TDStorage> currentStorage = [[TDLocalStorage alloc] init];
    return [currentStorage isExist:item];
}
-(void)getBigImagePreview:(TDItem *)item
{
    id<TDStorage> currentStorage = [[TDRemoteStorage alloc] init];
    [currentStorage getBigPreivew:item withResponder:self.responder];
}

#pragma mark - Manager Responder Delegate

//we switche self with current responder to control result

-(void)storageDidFinishLoadingAction:(id<TDAction>)action
{
    if ([action isKindOfClass:NSClassFromString(@"TDGetHierarchyRemote")]) {
        // We got an Objects successfully form server
        //lets Notify responder and Update current DB
        if (self.responder) {
            [self.responder storageDidFinishLoadingAction:action];
            //Update Local Storage
            dispatch_async(dispatch_get_main_queue(), ^(void)
            {
                id<TDStorage> currentStorage = [[TDLocalStorage alloc] init];
                [currentStorage update:[action item] withResponder:nil];
                               
            });
            
        }
    }
    
}

-(void)storageAction:(id<TDAction>)action FailWithError:(NSError *)error
{
    // We Fail getting an Objects successfully from server
    //Lets try to get Objects from local storage
    if ([action isKindOfClass:NSClassFromString(@"TDGetHierarchyRemote")]) {
        dispatch_async(dispatch_get_main_queue(), ^(void)
        {
        [self getHierarchy:[action item] withResponder:self.responder];
        });
    }
}



@end
