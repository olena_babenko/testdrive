//
//  TDFileViewController.h
//  TestDrive
//
//  Created by Olena Babenko on 2/9/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import "TDViewController.h"
#import "TDFile.h"

@interface TDFileViewController : TDViewController

@property (strong, nonatomic) UIImageView *file_img;
@property (strong, nonatomic) UILabel *size_lbl;
@property (strong, nonatomic) UILabel *type_lbl;
@property (strong, nonatomic) UILabel *created_lbl;
@property (strong, nonatomic) UILabel *modified_lbl;

@property (strong, nonatomic) TDFile *file;
@end
