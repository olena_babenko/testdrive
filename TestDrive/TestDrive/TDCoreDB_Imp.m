//
//  TDCoreDB_Imp.m
//  TestDrive
//
//  Created by Olena Babenko on 2/10/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import "TDCoreDB_Imp.h"

@interface TDCoreDB_Imp ()

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@end

@implementation TDCoreDB_Imp
@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

static TDCoreDB_Imp* sharedInstance = nil;

+ (TDCoreDB_Imp *)sharedInstance
{
    static dispatch_once_t once;
    dispatch_once(&once, ^{
        sharedInstance = [[super alloc] init];
    });
    return sharedInstance;
}

#pragma mark - Work with all Objects


-(NSArray *)getAllEntityObjects:(NSString *)entityName
{
    NSError* error = nil;
    NSMutableArray *results;
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:entityName];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"object_id" ascending:YES];
    results = [[[self managedObjectContext] executeFetchRequest:request error:&error] copy];
    [results sortedArrayUsingDescriptors:@[sortDescriptor]];
    if (error) {
        NSLog(@"%@", [error localizedDescription]);
    }
    return results;
}

-(void)deleteAllEntityObjects:(NSString *)entityName
{
    NSError* error = nil;
    NSFetchRequest* request = [[NSFetchRequest alloc] initWithEntityName:entityName];
    NSArray *allDataInEntity = [[self managedObjectContext] executeFetchRequest:request error:&error];
    for (NSManagedObject* object in allDataInEntity){
        [[self managedObjectContext] deleteObject:object];
    }
    if (error) {
        NSLog(@"%@", [error localizedDescription]);
        error = nil;
    }
    [self saveContext];
}


#pragma mark - Work with separate Object


-(NSManagedObject *)getObjectByField:(NSString *)field value:(id)data entity:(NSString *)entityName
{
   return [[self getAllObjectByField:field value:data entity:entityName] lastObject];
   

}
-(void)deleteObjectByField:(NSString *)field value:(id)data entity:(NSString *)entityName
{
    NSManagedObject* managedObg = [self getObjectByField:field value:data entity:entityName];
    if (managedObg) {
        [[self managedObjectContext] deleteObject:managedObg];
    }
    
    [self saveContext];
}
-(NSManagedObject *)createManagedObjectInEntity:(NSString *)entityName
{
    NSManagedObject *managedObject;
    
    NSEntityDescription *discr = [NSEntityDescription entityForName:entityName inManagedObjectContext:self.managedObjectContext];
    managedObject = [[NSManagedObject alloc] initWithEntity:discr insertIntoManagedObjectContext:self.managedObjectContext];
    
    return managedObject;
}


#pragma mark - Objects with same property. Work like a filter


-(NSArray *)getAllObjectByField:(NSString *)field value:(id)data entity:(NSString *)entityName
{
    NSArray *result;
    NSError *error;
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@",field,data];
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:entityName];
    [request setPredicate:predicate];
    result = [[self managedObjectContext] executeFetchRequest:request error:&error];
    if (error) {
     NSLog(@"get object Error %@", [error description]);   
    }
    return result;
}

-(void)deleteAllObjectByField:(NSString *)field value:(id)data entity:(NSString *)entityName
{
    NSArray *result = [self getAllObjectByField:field value:data entity:entityName];
    for (NSManagedObject* object in result){
        [[self managedObjectContext] deleteObject:object];
    }
    [self saveContext];
}


#pragma mark - Save Context


-(void)saveChanges
{
    [self saveContext];
}


#pragma mark - Core Data Privat


- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

#pragma mark - Core Data stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"TestDrive" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"TestDrive.sqlite"];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
       
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

@end
