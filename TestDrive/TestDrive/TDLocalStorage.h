//
//  TDLocalStorage.h
//  TestDrive
//
//  Created by Olena Babenko on 2/9/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TDStorage.h"

@interface TDLocalStorage : NSObject<TDStorage>



@end
