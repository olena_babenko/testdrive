//
//  TDViewController.m
//  TestDrive
//
//  Created by Olena Babenko on 2/9/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import "TDViewController.h"
#import "TDLoginViewController.h"

@interface TDViewController ()

@end

@implementation TDViewController


- (id)init
{
    self = [super init];
    if (self) {
        
        [self.view setBackgroundColor:[UIColor lightGrayColor]];
         [self.view addSubview:self.action];
        
    }
    return self;
}
- (UIActivityIndicatorView *)action
{
    if (!_action) {
        _action = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        _action.center = self.view.center;
        _action.hidden = YES;
    }
    return _action;
}
- (void)startLoading
{
    self.action.hidden = NO;
    [self.view bringSubviewToFront:self.action];
    [self.action startAnimating];
}
- (void)stopLoading
{
    self.action.hidden = YES;
    [self.action stopAnimating];
}

-(void)addNavigationToolBar
{
    //Add navigation toolbar
    if (self.navigationController.viewControllers[0] == self) {
        //if its rootView Controller
        NSString *token = [[NSUserDefaults standardUserDefaults] objectForKey:kTokenKey];
        if (!token) {
            UIBarButtonItem *logInButton = [[UIBarButtonItem alloc] initWithTitle:@"Вход" style:UIBarButtonItemStylePlain target:self action:@selector(login)];
            self.navigationItem.rightBarButtonItem = logInButton;
        }
    }else
    {       UIBarButtonItem *homeButton = [[UIBarButtonItem alloc] initWithTitle:@"Главная" style:   UIBarButtonItemStylePlain target:self action:@selector(toRootView)];
        self.navigationItem.rightBarButtonItem = homeButton;
        
    }
}

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    NSString *token = [[NSUserDefaults standardUserDefaults] objectForKey:kTokenKey];
    if (!token) {
       UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Аккаунт не установлен" message:nil delegate:self cancelButtonTitle:@"Подключить" otherButtonTitles:@"Отменить", nil];
        [alert show];
    }
	// Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self addNavigationToolBar];
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        [self login];
    }
}
-(void)login
{
    TDLoginViewController *login = [[TDLoginViewController alloc] init];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:login];
    [self.navigationController presentViewController:nav animated:NO completion:nil];
}
-(void)toRootView
{
    [self.navigationController popToRootViewControllerAnimated:NO];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
