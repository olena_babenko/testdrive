//
//  TDExistanseLocal.m
//  TestDrive
//
//  Created by Olena Babenko on 2/10/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import "TDExistanseLocal.h"
#import "TDLocalStorageAction_Privat.h"

@implementation TDExistanseLocal


-(BOOL)checkItem:(TDItem *)item
{
    if ([self getManagedObjectByItem:item]) {
        return YES;
    }
    return NO;
}

@end
