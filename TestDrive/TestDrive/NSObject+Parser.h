//
//  NSObject+Parser.h
//  TestDrive
//
//  Created by Olena Babenko on 2/9/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (Parser)

-(NSDate *)creationDate;
-(NSDate *)modifiedDate;
-(NSString *)string;
-(NSNumber *)number;
-(NSInteger)integer;
-(long long)longlong;
-(BOOL)isFolder;
-(BOOL)statusSuccess;


@end
