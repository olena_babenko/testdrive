//
//  TDItemCell.m
//  TestDrive
//
//  Created by Olena Babenko on 2/8/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import "TDItemCell.h"
#import "UILabel+Style.h"

@implementation TDItemCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        [self addSubview:self.icon_lbl];
        [self addSubview:self.name_lbl];
        [self addSubview:self.lastModified_lbl];
        self.selectionStyle = UITableViewCellSelectionStyleGray;
    }
    return self;
}
- (UIImageView *)icon_lbl
{
    if (!_icon_lbl) {
        
        CGRect icon_frame = CGRectMake(10, 3, 30, 30);
        _icon_lbl = [[UIImageView alloc] initWithFrame:icon_frame];
        [_icon_lbl setContentMode:UIViewContentModeScaleAspectFit];
    }
    return _icon_lbl;
}
- (UILabel *)name_lbl
{
    if (!_name_lbl) {
        CGRect align = self.icon_lbl.frame;
        //make offset a bit by X
        CGFloat align_max_x =CGRectGetMaxX(align) + 2;
        CGRect name_frame = CGRectMake(align_max_x, align.origin.y, self.frame.size.width-align_max_x, align.size.height);
        
        _name_lbl = [UILabel labelforCellTitleWithFrame:name_frame];
    }
    return _name_lbl;
}
- (UILabel *)lastModified_lbl
{
    if (!_lastModified_lbl) {
        CGRect align = self.name_lbl.frame;
        CGRect time_frame = CGRectMake(align.origin.x, CGRectGetMaxY(align), align.size.width, 15);
        //make offset a bit by Y
        time_frame = CGRectOffset(time_frame, 0, 2);
        _lastModified_lbl = [UILabel labelforCellSubtitleWithFrame:time_frame];
    }
    return _lastModified_lbl;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)setData:(TDItem *)data
{
    _data = data;
    self.name_lbl.text = data.name;
    self.lastModified_lbl.text = data.lastMofifiedString;
    
}
@end
