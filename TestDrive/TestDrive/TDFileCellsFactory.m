//
//  TDFileCellsFactory.m
//  TestDrive
//
//  Created by Olena Babenko on 2/8/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import "TDFileCellsFactory.h"
#import "TDFileCell.h"

@implementation TDFileCellsFactory


-(UITableViewCell *)cellForTable:(UITableView *)table andData:(id)data indexPath:(NSIndexPath *)indexPath
{
    static NSString *fileCellIdentifier = @"TDFileCell";
    TDFileCell *cell = [table dequeueReusableCellWithIdentifier:fileCellIdentifier];
    
    if (cell == nil) {
        cell = [[TDFileCell alloc] initWithStyle:UITableViewCellStyleDefault  reuseIdentifier:fileCellIdentifier];
    }
    cell.data = data;
    
    return cell;
}
-(CGFloat)cellHeightForData:(id)data
{
    TDFile *data_file = data;
    if ([data_file isKindOfClass:[TDFile class]]) {
        if ([data_file sizeHidden]) {
            return 56;
        }
        return 75;
    }
    return 40;
}

@end
