//
//  TDFile.h
//  TestDrive
//
//  Created by Olena Babenko on 2/7/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import "TDItem.h"

@interface TDFile : TDItem


@property (strong, nonatomic) UIImage *file_img;

-(NSURL *)fullPath;
-(BOOL)isImageContent;
@end
