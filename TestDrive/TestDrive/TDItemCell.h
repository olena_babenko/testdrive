//
//  TDItemCell.h
//  TestDrive
//
//  Created by Olena Babenko on 2/8/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TDItem.h"

@interface TDItemCell : UITableViewCell

@property (strong, nonatomic) UILabel *name_lbl;
@property (strong, nonatomic) UILabel *lastModified_lbl;
@property (strong, nonatomic) UIImageView *icon_lbl;

@property (weak, nonatomic) TDItem *data;

@end
