//
//  TDViewController.h
//  TestDrive
//
//  Created by Olena Babenko on 2/9/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TDViewController : UIViewController<UIAlertViewDelegate>

@property (strong, nonatomic) UIActivityIndicatorView *action;

- (void)startLoading;
- (void)stopLoading;

@end
