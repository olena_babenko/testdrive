//
//  TDPicturePreview.m
//  TestDrive
//
//  Created by Olena Babenko on 2/11/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import "TDPicturePreviewRemote.h"
#import "TDRemoteStorageAction_Privat.h"
#import "TDFile.h"

@implementation TDPicturePreviewRemote

-(NSString *)method
{
    return @"GET";
}

-(NSURL *)path
{
    NSURL *_path = [super path];
    if (self.item.localPath) {
        
        _path = [_path URLByAppendingPathComponent:self.item.localPath];
        NSString *previewSettings = [NSString stringWithFormat:@"%@?preview&size=M", _path.absoluteString ];
        _path = [NSURL URLWithString:previewSettings];
    }
    return _path;
}

-(void)addHeadersToRequest:(NSMutableURLRequest *)request
{
    //add AUTH header
    [super addHeadersToRequest:request];
}

-(void)parseData:(NSData *)data
{
   
     UIImage *img =  [UIImage imageWithData:data];
    if ([img isKindOfClass:[UIImage class]]) {
        NSLog(@"Its an Image!!");
        TDFile *current_file;
        if ([self.item isKindOfClass:[TDFile class]]) {
            current_file = (TDFile *)self.item;
            current_file.file_img = img;
        }
    }
    [super completeRequestWithAnswer:img];
    
}

@end
