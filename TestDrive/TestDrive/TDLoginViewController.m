//
//  TDLoginViewController.m
//  TestDrive
//
//  Created by Olena Babenko on 2/8/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import "TDLoginViewController.h"

@interface TDLoginViewController ()<TDAuthDelegate>

@end

@implementation TDLoginViewController


- (id)init
{
    self = [super initWithClientID:@"72fa3dca6714499eadba69872cbcd14e" redirectPath:@"testdriveapp://token" delegate:self];
    if (self) {
        [self.navigationController.navigationBar setTranslucent:NO];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - TDAuth Delegate


- (void)authDidEndWithToken:(NSString *)token
{
    NSLog(@"Token %@", token);
    [[NSUserDefaults standardUserDefaults] setObject:token forKey:kTokenKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void) authDidFailWithError:(NSError *)error
{
    NSLog(@"Error %@", error);
    [self dismissViewControllerAnimated:YES completion:nil];
}


@end
