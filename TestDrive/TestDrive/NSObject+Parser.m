//
//  NSObject+Parser.m
//  TestDrive
//
//  Created by Olena Babenko on 2/9/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import "NSObject+Parser.h"

@implementation NSObject (Parser)

-(NSDate *)creationDate
{
    NSString * time =[self string];
    if (time) {
        NSDateFormatter* fmt = [[NSDateFormatter alloc] init];
        //[fmt setLocale:[NSLocale systemLocale]];
        [fmt setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
        [fmt setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
        NSDate* date = [fmt dateFromString:time];
        return date;
    }
    return nil;
}
-(NSDate *)modifiedDate
{
    NSString * time =[self string];
    if (time) {
        NSDateFormatter* fmt = [[NSDateFormatter alloc] init];
        //[fmt setLocale:[NSLocale systemLocale]];
        [fmt setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
        [fmt setDateFormat:@"EEE, dd MMM yyyy HH:mm:ss Z"];
        NSDate* date = [fmt dateFromString:time];
        return date;
    }
    return nil;
}
-(NSString *)string
{
    if (![self isKindOfClass:[NSNull class]] && self) {
        return [NSString stringWithFormat:@"%@",self];
    }
    return nil;
    
}
-(NSNumber *)number
{
    if ([self isKindOfClass:[NSNumber class]]) {
        return (NSNumber *)self;
    }
    return nil;
}
-(NSInteger)integer
{
    return [self number].integerValue;
    
}
-(long long)longlong
{
    return [self number].longLongValue;
}
-(BOOL)isFolder
{
    if ([self isKindOfClass:[NSDictionary class]]) {
        NSDictionary *self_dict = (NSDictionary *)self;
        if (self_dict[@"d:collection"]) {
            return YES;
        }
    }
    return NO;
}
-(BOOL)statusSuccess
{
    NSString * status =[self string];
    if ([status isEqualToString:@"HTTP/1.1 200 OK"]) {
        return YES;
    }
    return NO;
}
@end
