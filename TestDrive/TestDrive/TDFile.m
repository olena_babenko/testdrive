//
//  TDFile.m
//  TestDrive
//
//  Created by Olena Babenko on 2/7/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import "TDFile.h"
#import "TDItem+CoreData.h"

@implementation TDFile

-(NSURL *)fullPath
{
   NSURL *standart_path = [NSURL URLWithString:@"https://webdav.yandex.ru:443"];
   return [standart_path URLByAppendingPathComponent:self.localPath];
}
-(BOOL)isImageContent
{
    if ([self.contentType isEqualToString:@"image/jpeg"]) {
        return YES;
    }
    if ([self.contentType isEqualToString:@"image/png"]) {
        return YES;
    }
    return NO;
}
-(void)updateWithCurrentItemManagedObject:(TDItemMO *)managedObject
{
    [super updateWithCurrentItemManagedObject:managedObject];
   managedObject.is_folder = [NSNumber numberWithInt:0];
}
@end
