//
//  TDFileCell.m
//  TestDrive
//
//  Created by Olena Babenko on 2/8/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import "TDFileCell.h"
#import "UILabel+Style.h"


@implementation TDFileCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        [self addSubview:self.size_lbl];
        self.contentView.backgroundColor = [UIColor colorWithRed:25/255.0 green:35/255.0  blue:65/255.0  alpha:0.3];
    }
    return self;
}

- (UILabel *)size_lbl
{
    if (!_size_lbl) {
        //alight size to last Modified lbl
        CGRect align = self.lastModified_lbl.frame;
        CGRect size_frame = CGRectMake(align.origin.x, CGRectGetMaxY(align), align.size.width, 15);
        //make offset a bit by Y
        size_frame = CGRectOffset(size_frame, 0, 2);
        _size_lbl = [UILabel labelforCellSubtitleWithFrame:size_frame];
    }
    return _size_lbl;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

//заполнить данные
-(void)setData:(TDItem *)data
{
    if ([data isKindOfClass:[TDFile class]]) {
        [super setData:data];
        TDFile *localData = (TDFile *)data;
        self.size_lbl.text = localData.sizeString;
        self.size_lbl.hidden = [localData sizeHidden];
        self.icon_lbl.image = [UIImage imageNamed:@"file.png"];
    }
}



@end
