//
//  TDItem+CoreData.m
//  TestDrive
//
//  Created by Olena Babenko on 2/10/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import "TDItem+CoreData.h"

#import "TDRootFolder.h"
#import "TDFile.h"
#import "TDItem_Privat.h"

@implementation TDItem (CoreData)

+(instancetype)itemWithManagedObject:(TDItemMO *)managedObject
{
    if (!managedObject.parent_id) {
        return [[TDRootFolder alloc] initWithManagedObject:managedObject];
    }
    //lets get Class Пока что только 2 класса
    BOOL isFolder = [[managedObject is_folder] boolValue];
    NSLog(@"Folder info %@", [managedObject description]);
    if (isFolder) {
        NSLog(@"Created Folder");
        return [[TDFolder alloc] initWithManagedObject:managedObject];
    }
    return [[TDFile alloc] initWithManagedObject:managedObject];
}
-(instancetype)initWithManagedObject:(TDItemMO*)managedObject
{
    self = [super init];
    if (self) {
        [self updateWithManagedObject:managedObject];
    }
    return self;
}

-(void)updateWithManagedObject:(TDItemMO *)managedObject
{
    self.localPath = [managedObject localPath];
    self.name = [managedObject name];
    self.lastModified = [managedObject lastModified];
    self.created = [managedObject creation];
    self.contentType = [managedObject contentType];
    self.size = [[managedObject size] longLongValue];
    
}

-(void)updateWithCurrentItemManagedObject:(TDItemMO *)managedObject
{

    managedObject.localPath = self.localPath;
    managedObject.name = self.name;
    managedObject.lastModified = self.lastModified;
    managedObject.creation = self.created;
    managedObject.contentType = self.contentType;
    managedObject.size = [NSNumber numberWithLongLong:self.size];
    
    
    
}

@end
