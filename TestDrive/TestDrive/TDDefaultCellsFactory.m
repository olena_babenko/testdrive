//
//  TDDefaultCellsFactory.m
//  TestDrive
//
//  Created by Olena Babenko on 2/8/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import "TDDefaultCellsFactory.h"

@implementation TDDefaultCellsFactory

-(UITableViewCell *)cellForTable:(UITableView *)table andData:(id)data indexPath:(NSIndexPath *)indexPath
{
    static NSString *defaultCellIdentifier = @"UIDefaultCell";
    UITableViewCell *cell = [table dequeueReusableCellWithIdentifier:defaultCellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault  reuseIdentifier:defaultCellIdentifier];
    }

    return cell;
}
-(CGFloat)cellHeightForData:(id)data
{
   
    return 30;
}
@end
