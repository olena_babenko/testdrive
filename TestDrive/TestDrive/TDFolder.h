//
//  TDFolder.h
//  TestDrive
//
//  Created by Olena Babenko on 2/7/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import "TDItem.h"

@interface TDFolder : TDItem




@property(nonatomic, readonly, strong) NSArray *subItems;



-(void)replaceItems:(NSArray *)subItems;
-(void)clearAllImages;
@end
