//
//  NSError+StorageError.h
//  TestDrive
//
//  Created by Olena Babenko on 2/9/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSError (StorageError)

+(NSError *)errorStorageWithCode:(NSInteger)code andDescription:(NSString *)localDescription;

@end
