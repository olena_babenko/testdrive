//
//  TDItem+Remote.h
//  TestDrive
//
//  Created by Olena Babenko on 2/10/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import "TDItem.h"

@interface TDItem (Remote)

+(instancetype)itemWithRemoteDictionary:(NSDictionary *)dict;
@end
