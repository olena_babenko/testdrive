//
//  TDFolder.m
//  TestDrive
//
//  Created by Olena Babenko on 2/7/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import "TDFolder.h"
#import "TDItem+CoreData.h"
#import "TDFile.h"

@interface TDFolder ()
{
    NSArray *items;
}


@end


@implementation TDFolder

-(NSArray *)subItems
{
    return items;
}

-(void)updateWithCurrentItemManagedObject:(TDItemMO *)managedObject
{
    [super updateWithCurrentItemManagedObject:managedObject];
    managedObject.is_folder = [NSNumber numberWithInt:1];
}
-(void)replaceItems:(NSArray *)subItems
{
    items = subItems;
}
-(void)clearAllImages
{
    for (TDItem *item in items) {
        if ([item isKindOfClass:[TDFile class]]) {
            TDFile *file = (TDFile *)item;
            file.file_img = nil;
        }
    }
}
@end
