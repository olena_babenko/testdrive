//
//  TDItemData.h
//  TestDrive
//
//  Created by Olena Babenko on 2/9/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TDStorage.h"



//Этот класс решает откуда брать данные - из локального хранилища или из удаленного сервера 
@interface TDStorageManager : NSObject


-(id)initWithResponder:(id<TDStorageResponder>)responder;

-(void)getHierarchy:(TDItem *)item;
-(void)updateLocal:(TDItem *)item;
-(void)getBigImagePreview:(TDItem *)item;

@end
