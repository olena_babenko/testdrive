//
//  TDRemoteStorageAction.h
//  TestDrive
//
//  Created by Olena Babenko on 2/8/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TDItem.h"
#import "TDStorageResponder.h"
#import "TDAction.h"

@interface TDRemoteStorageAction : NSObject<NSURLConnectionDelegate, NSURLConnectionDataDelegate, TDAction>


@end
