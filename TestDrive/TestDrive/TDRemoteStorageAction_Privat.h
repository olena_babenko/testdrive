//
//  TDRemoteStorageAction_Privat.h
//  TestDrive
//
//  Created by Olena Babenko on 2/8/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import "TDRemoteStorageAction.h"

@interface TDRemoteStorageAction ()
{

}
@property(strong, nonatomic) TDItem *item;
@property(nonatomic, weak) id<TDStorageResponder> responder;

-(NSURL *)path;
-(NSData *)data;
-(NSString *)method;
-(void)addHeadersToRequest:(NSMutableURLRequest *)request;
// can be rewritten on every step
-(void)parseData:(NSData *)data;
-(id)parseResponse:(id)response;
-(void)completeRequestWithAnswer:(id)response;


@end
