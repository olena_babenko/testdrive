//
//  TDGetHierarchyAction.m
//  TestDrive
//
//  Created by Olena Babenko on 2/8/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import "TDGetHierarchyRemote.h"
#import "TDRemoteStorageAction_Privat.h"
#import "TDFolder.h"
#import "TDItem+Remote.h"

@interface TDGetHierarchyRemote ()



@end

@implementation TDGetHierarchyRemote


-(NSString *)method
{
    return @"PROPFIND";
}

-(NSURL *)path
{
    NSURL *_path = [super path];
    if (self.item.localPath) {
        
        _path = [_path URLByAppendingPathComponent:self.item.localPath];
    }
   return _path;
}

-(void)addHeadersToRequest:(NSMutableURLRequest *)request
{
    //add AUTH header
    [super addHeadersToRequest:request];
    [request setValue:@"*/*" forHTTPHeaderField:@"Accept"];
    [request setValue:@"1" forHTTPHeaderField:@"Depth"];
}

-(id)parseResponse:(id)response
{
    
    //[super parseResponse:response];
    //it should be an array of Dictionaries
    NSArray *items =  (NSArray *)response;

    NSMutableArray *subItems = [[NSMutableArray alloc] init];
    
    if ([items isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in items) {
            TDItem *created_item = [TDItem itemWithRemoteDictionary:item];
            if (created_item) {
                if (![created_item.localPath isEqualToString:self.item.localPath]) {
                    //в ответе сервера первым может идти текущий элемент, его не будем добавлять
                    //Add result to item
                    [subItems addObject:created_item];
                }
            }
        }//Result in SubItems
        return [subItems copy];
    }//Ассерт я поставила, чтобы мгновенно найти ошибку, если серверная сторона отдает ответ в другом формате
    else
        NSAssert(NO, @"Unexpected ROOT Parce type in %@. Should be an Array", [self class]);
    return nil;
}
-(void)completeRequestWithAnswer:(id)response
{
    if ([response isKindOfClass:[NSArray class]]) {
        //it should answer with subitems
        //Если мы смотрим папку, то должны заменить ее содержимое
        TDFolder *current_folder;
        if ([self.item isKindOfClass:[TDFolder class]]) {
            current_folder = (TDFolder *)self.item;
            [current_folder replaceItems:(NSArray *)response];
        }
    }
    [super completeRequestWithAnswer:response];
}
@end
