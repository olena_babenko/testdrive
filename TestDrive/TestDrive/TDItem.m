//
//  TDItem.m
//  TestDrive
//
//  Created by Olena Babenko on 2/7/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import "TDItem.h"
#import "objc/runtime.h"

#import "TDFolder.h"
#import "TDFile.h"
#import "TDItem_Privat.h"

#define minFileSize 10000

#define SIZE_IN_KB(x) x/1000.0
#define SIZE_IN_MB(x) x/1000000.0
#define SIZE_IN_GB(x) x/1000000000.0



@implementation TDItem

/*
// only for debugging - show mapping result
- (void)debugMethod {
    unsigned int outCount, i;
    objc_property_t *properties = class_copyPropertyList([self class], &outCount);
    for(i = 0; i < outCount; i++) {
        objc_property_t property = properties[i];
        const char *propName = property_getName(property);
        if(propName) {
        
            NSString *propertyName = [NSString stringWithCString:propName
                                                        encoding:[NSString defaultCStringEncoding]];
            NSLog(@"NAME: %@\n VALUE:  %@\n", propertyName, [self valueForKey:propertyName]);
        }
    }
    free(properties);
}*/

-(NSDateFormatter *)formatter
{
    NSDateFormatter* fmt = [[NSDateFormatter alloc] init];
    [fmt setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    [fmt setDateFormat:@"d MMM yyyy  HH:mm:ss"];
    return fmt;
}

-(NSString *)lastMofifiedString
{
    if (self.lastModified) {
        return  [[self formatter] stringFromDate:self.lastModified];
    }
    return nil;
}
-(NSString *)createdString
{
    if (self.created) {
         return  [[self formatter] stringFromDate:self.created];
    }
    return nil;
}

-(NSString *)sizeString
{
    if (![self sizeHidden]) {
        [self sizeStringAsIs];
    }
    return nil;
}
-(NSString *)sizeStringAsIs
{
    long long size = self.size;
    NSString *size_string;
    if (SIZE_IN_MB(size)<0.1) {
        size_string =[NSString stringWithFormat:@"%0.2f Kb", SIZE_IN_KB(size)];
    }else
    {
        if (SIZE_IN_GB(size)<0.1) {
            size_string =[NSString stringWithFormat:@"%0.2f Mb",SIZE_IN_MB(size)];
        }else
            size_string =[NSString stringWithFormat:@"%0.2f Gb",SIZE_IN_GB(size)];
    }
    return [NSString stringWithFormat:@"Size: %@", size_string];
}
-(BOOL)sizeHidden
{
    if (self.size<minFileSize) {
        return YES;
    }
    return NO;
}
@end
