//
//  TDFolderCellsFactory.m
//  TestDrive
//
//  Created by Olena Babenko on 2/8/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import "TDFolderCellsFactory.h"
#import "TDFolderCell.h"

@implementation TDFolderCellsFactory

-(UITableViewCell *)cellForTable:(UITableView *)table andData:(id)data indexPath:(NSIndexPath *)indexPath
{
    static NSString *folderCellIdentifier = @"TDFolderCell";
    TDFolderCell *cell = [table dequeueReusableCellWithIdentifier:folderCellIdentifier];
    
    if (cell == nil) {
        cell = [[TDFolderCell alloc] initWithStyle:UITableViewCellStyleDefault  reuseIdentifier:folderCellIdentifier];
    }
    cell.data = data;
    
    return cell;
}
-(CGFloat)cellHeightForData:(id)data
{
    return 56;
}

@end
