//
//  TDAction.h
//  TestDrive
//
//  Created by Olena Babenko on 2/10/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import <Foundation/Foundation.h>

@class TDItem;
@protocol TDAction <NSObject>


-(void)runItem:(TDItem *)item withResponder:(id)responder;
-(TDItem *)item;

@end
