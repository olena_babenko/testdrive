//
//  NSError+StorageError.m
//  TestDrive
//
//  Created by Olena Babenko on 2/9/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import "NSError+StorageError.h"

@implementation NSError (StorageError)

+(NSError *)errorStorageWithCode:(NSInteger)code andDescription:(NSString *)localDescription
{
    NSMutableDictionary* details = [NSMutableDictionary dictionary];
    [details setValue:localDescription forKey:NSLocalizedDescriptionKey];
    return [NSError errorWithDomain:@"drive.storage.error" code:code userInfo:details];
}
@end
