//
//  TDAuthViewController.m
//  SampleRequest
//
//  Created by Olena Babenko on 2/10/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import "TDAuthViewController.h"

#define AUTH_PATH  @"https://oauth.yandex.ru/authorize?response_type=token&client_id=%@&display=popup"

@interface TDAuthViewController ()<UIWebViewDelegate>

@property (strong,nonatomic) NSString *clientID;
@property (strong,nonatomic) NSString *redirectPath;
@property (strong,nonatomic) UIWebView *webView;

@property (assign,nonatomic) BOOL isFinished;

@end

@implementation TDAuthViewController

- (id)initWithClientID:(NSString *)clientID redirectPath:(NSString *)redirect delegate:(id<TDAuthDelegate>)auth_delegate
{
    self = [super init];
    if (self) {
        _clientID = clientID;
        _redirectPath = redirect;
        _delegate = auth_delegate;
        self.title = @"Вход";
    }
    return self;
}
- (UIWebView *)webView
{
    if (!_webView) {
        _webView = [[UIWebView alloc] init];
        _webView.delegate = self;
        _webView.frame = self.view.bounds;
    }
    return _webView;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSURL *authURL = [self authURL];
    if (authURL) {
        //Единственное вью которое нужно для сторонней авторизации
        [self.view addSubview:self.webView];
        NSURLRequest *authRequest = [NSURLRequest requestWithURL:authURL];
        [self.webView loadRequest:authRequest];
        
    }else
        [self authFailedWithError:[self authErrorWithCode:400 andDescription:@"No clientID OR redirect path"]];
   
}
- (NSURL *)authURL
{
    if (self.clientID && self.redirectPath) {
        
        NSString *authPath = [NSString stringWithFormat:AUTH_PATH,self.clientID];
        return [NSURL URLWithString:authPath];
    }
    return nil;
}


#pragma mark - UIWebViewDelegate methods


- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    NSString *answerPath = request.URL.absoluteString;
    if ([self isEndPath:answerPath]) {
        
        NSDictionary *answerProperties = [self findPropertiesFromPath:answerPath];
        if (answerProperties[@"access_token"]) {
            //Request Completed Successfully
            [self authCompleteWithToken:answerProperties[@"access_token"]];
            self.isFinished = YES;
            return NO;
        }
        if (answerProperties[@"error"]) {
            //Request Completed BUT with Error
            [self authFailedWithError:[self authErrorWithCode:400 andDescription:answerProperties[@"error"]]];
            self.isFinished = YES;
            return NO;
        }
        
    }
    return YES;
}
- (BOOL)isEndPath:(NSString *)answerPath
{
    if ([answerPath hasPrefix:self.redirectPath]) {
        return YES;
    }
    return NO;
}
- (NSDictionary *)findPropertiesFromPath:(NSString *)answerPath
{
    //lets find only part with properties
    NSArray *pathComponents = [answerPath componentsSeparatedByString:@"#"];
    //to be sure, that we have properties part
    if (pathComponents.count>1) {
        NSArray *propertiesStrings = [pathComponents[1] componentsSeparatedByString:@"&"];
        NSMutableDictionary *properties  = [[NSMutableDictionary alloc] init];
        
        for (NSString *propertyString in propertiesStrings) {
            NSArray *propertyParts = [propertyString componentsSeparatedByString:@"="];
            if (propertyParts.count>1) {
                [properties setValue:propertyParts[1] forKey:propertyParts[0]];
            }
        }
        
        return properties;
        
    }
    return nil;
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    if (!self.isFinished) {
        [self authFailedWithError:error];
    }
}


#pragma mark - Results


- (void)authFailedWithError:(NSError *)error
{
    if (self.delegate) {
        [self.delegate authDidFailWithError:error];
    }
}
- (void)authCompleteWithToken:(NSString *)token
{
    if (self.delegate) {
        [self.delegate authDidEndWithToken:token];
    }
}
-(NSError *)authErrorWithCode:(NSInteger)code andDescription:(NSString *)description
{
    NSMutableDictionary* details = [NSMutableDictionary dictionary];
    [details setValue:description forKey:NSLocalizedDescriptionKey];
    return [NSError errorWithDomain:@"com.auth.error" code:code userInfo:details];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
