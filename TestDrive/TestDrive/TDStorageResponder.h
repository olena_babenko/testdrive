//
//  TDStorageResponder.h
//  TestDrive
//
//  Created by Olena Babenko on 2/9/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TDAction.h"

@protocol TDStorageResponder <NSObject>

-(void)storageAction:(id<TDAction>)action FailWithError:(NSError *)error;
-(void)storageDidFinishLoadingAction:(id<TDAction>)action;



@end
