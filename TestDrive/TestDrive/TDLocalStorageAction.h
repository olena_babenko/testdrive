//
//  TDLocalStorageAction.h
//  TestDrive
//
//  Created by Olena Babenko on 2/8/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TDStorageResponder.h"
#import "TDItem.h"
#import "TDCoreDB_Imp.h"
#import "TDItemMO.h"
#import "TDAction.h"

@interface TDLocalStorageAction : NSObject<TDAction>



@end
