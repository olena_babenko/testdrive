//
//  TDItem.h
//  TestDrive
//
//  Created by Olena Babenko on 2/7/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface TDItem : NSObject

@property(nonatomic, readonly, strong) NSString *name;
@property(nonatomic, readonly, strong) NSString *localPath;
@property(nonatomic, readonly, strong) NSURL *publicURL;
@property(nonatomic, readonly, strong) NSDate *lastModified;
@property(nonatomic, readonly, strong) NSDate *created;
@property(nonatomic, readonly, strong) NSString *contentType;

@property(nonatomic, readonly, assign) long long size;
@property(nonatomic, readonly, assign) BOOL isReadOnly;
@property(nonatomic, readonly, assign) BOOL isloadSuccesfully;






-(NSString *)sizeString;
-(NSString *)sizeStringAsIs;
-(NSString *)lastMofifiedString;
-(NSString *)createdString;

-(BOOL)sizeHidden;

@end
