//
//  TDUpdateLocal.m
//  TestDrive
//
//  Created by Olena Babenko on 2/10/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import "TDUpdateLocal.h"
#import "TDLocalStorageAction_Privat.h"
#import "TDRootFolder.h"
#import "TDItem+CoreData.h"

@implementation TDUpdateLocal

//Item - currentFolder
-(void)runItem:(TDItem *)item withResponder:(id<TDStorageResponder>)responder
{
    [self updateItem:item withParent:nil];
     //Lets Save all Changes
     [[TDCoreDB_Imp sharedInstance] saveChanges];
    
    if (responder) {
        [responder storageDidFinishLoadingAction:self];
    }
    
    
}

-(void)updateItem:(TDItem *)item withParent:(TDFolder *)parent
{
    //Get managed object if exist
    TDItemMO *itemMO = [self getManagedObjectByItem:item];
    //If there is no such Data in DB lets create it
    if (!itemMO) {
        itemMO = [self createManageObjectWithParentItem:parent];
    }else
    {   //If we created Object Without Parent reference - lets update it
        if (parent&&!itemMO.parent_id) {
         itemMO.parent_id = parent.localPath;
        }
    }
   
    //Now we have MO that we Can set Data to This object
    [item updateWithCurrentItemManagedObject:itemMO];
    
    //lets check if we have subItems
    if ([item isKindOfClass:[TDFolder class]]) {
        NSArray *subItems = [((TDFolder *)item) subItems];
        for (TDItem *subItem in subItems) {
            [self updateItem:subItem withParent:((TDFolder *)item)];
        }
    }
}

@end
