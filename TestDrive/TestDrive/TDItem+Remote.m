//
//  TDItem+Remote.m
//  TestDrive
//
//  Created by Olena Babenko on 2/10/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import "TDItem+Remote.h"
#import "TDItem_Privat.h"
#import "TDRootFolder.h"
#import "TDFile.h"


#import "NSObject+Parser.h"

@implementation TDItem (Remote)

+(instancetype)itemWithRemoteDictionary:(NSDictionary *)dict
{
    //Check for successfully load of current object
    BOOL isloadSuccesfully = [dict[@"d:propstat"][@"d:status"] statusSuccess] ;
    if (isloadSuccesfully) {
        //lets get Class Пока что только 2 класса
        BOOL isFolder = [dict[@"d:propstat"][@"d:prop"][@"d:resourcetype"] isFolder];
        if (isFolder) {
            return [[TDFolder alloc] initWithRemoteDictionary:dict];
        }
        return [[TDFile alloc] initWithRemoteDictionary:dict];
        
    }NSLog(@"Loading failed %@", [dict[@"d:href"] string]);
    return nil;
}
-(instancetype)initWithRemoteDictionary:(NSDictionary *)dict
{
    self = [super init];
    if (self) {
        
        //Standart Parser
        self.localPath = [dict[@"d:href"] string];
        self.localPath = [self.localPath stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        self.isloadSuccesfully = [dict[@"d:propstat"][@"d:status"] statusSuccess] ;
        if (self.isloadSuccesfully) {
            
            NSDictionary *properties  = dict[@"d:propstat"][@"d:prop"];
            if (properties) {
                self.name = [properties[@"d:displayname"] string];
                self.lastModified = [properties[@"d:getlastmodified"] modifiedDate];
                self.created = [properties[@"d:creationdate"] creationDate];
                self.contentType = [properties[@"d:getcontenttype"] string];
                self.size = [properties[@"d:getcontentlength"] longLongValue];
            }
        }else
            NSLog(@"Load failed %@", self.localPath);
    }
    return self;
}


@end
