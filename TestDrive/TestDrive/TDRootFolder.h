//
//  TDRootFolder.h
//  TestDrive
//
//  Created by Olena Babenko on 2/7/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import "TDFolder.h"
#import "TDItem_Privat.h"

@interface TDRootFolder : TDFolder


+(TDFolder *)rootFolder;

@end
