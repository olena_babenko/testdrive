//
//  TDExistanseLocal.h
//  TestDrive
//
//  Created by Olena Babenko on 2/10/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import "TDLocalStorageAction.h"

@interface TDExistanseLocal : TDLocalStorageAction

-(BOOL)checkItem:(TDItem *)item;
@end
