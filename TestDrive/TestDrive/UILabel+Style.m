//
//  UILabel+Style.m
//  TestDrive
//
//  Created by Olena Babenko on 2/9/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import "UILabel+Style.h"

@implementation UILabel (Style)

+(UILabel *)labelforCellTitleWithFrame:(CGRect)frame
{
   UILabel *big_lbl =  [[UILabel alloc] initWithFrame:frame];
    big_lbl.backgroundColor = [UIColor clearColor];
    big_lbl.font =[UIFont systemFontOfSize:15.0];
    return big_lbl;
}
+(UILabel *)labelforCellSubtitleWithFrame:(CGRect)frame
{
    UILabel *middle_lbl = [[UILabel alloc] initWithFrame:frame];
    middle_lbl.backgroundColor = [UIColor clearColor];
    middle_lbl.font = [UIFont systemFontOfSize:13.0];
    middle_lbl.textColor =[UIColor darkGrayColor];
    return middle_lbl;
}
+(UILabel *)labelforFolderWarningWithFrame:(CGRect)frame
{
    UILabel *folder_lbl = [[UILabel alloc] initWithFrame:frame];
    folder_lbl.backgroundColor = [UIColor lightGrayColor];
    folder_lbl.textAlignment = NSTextAlignmentCenter;
    folder_lbl.textColor = [UIColor darkGrayColor];
    return folder_lbl;
    
}//Если лейблы будут повторяться их можно вынесть в отдельную функцию
+(UILabel *)labelforFilePropertiesWithFrame:(CGRect)frame
{
    UILabel *middle_lbl = [[UILabel alloc] initWithFrame:frame];
    middle_lbl.backgroundColor = [UIColor clearColor];
    middle_lbl.font = [UIFont systemFontOfSize:15.0];
    middle_lbl.textColor =[UIColor darkGrayColor];
    return middle_lbl;
}
@end
