//
//  TDCoreDB_Imp.h
//  TestDrive
//
//  Created by Olena Babenko on 2/10/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#pragma mark - Work with separate Objectimport <Foundation/Foundation.h>

@interface TDCoreDB_Imp : NSObject

-(NSArray *)getAllEntityObjects:(NSString *)entityName;
-(void)deleteAllEntityObjects:(NSString *)entityName;

//Only for Unique Objects
-(NSManagedObject *)getObjectByField:(NSString *)field value:(id)data entity:(NSString *)entityName;
-(void)deleteObjectByField:(NSString *)field value:(id)data entity:(NSString *)entityName;

//Objects Filers
-(NSArray *)getAllObjectByField:(NSString *)field value:(id)data entity:(NSString *)entityName;
-(void)deleteAllObjectByField:(NSString *)field value:(id)data entity:(NSString *)entityName;

-(NSManagedObject *)createManagedObjectInEntity:(NSString *)entityName;

+ (TDCoreDB_Imp *)sharedInstance;
-(void)saveChanges;

@end
