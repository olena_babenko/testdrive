//
//  TDFolderViewController.m
//  TestDrive
//
//  Created by Olena Babenko on 2/7/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import "TDFolderViewController.h"
#import "TDItem.h"

#import "TDFolderCellsFactory.h"
#import "TDFileCellsFactory.h"
#import "TDDefaultCellsFactory.h"

#import "TDStorageManager.h"
#import "TDStorageResponder.h"

#import "UILabel+Style.h"

#import "TDFileViewController.h"

#define kNoDataString @"Нет данных"

@interface TDFolderViewController ()<UITableViewDataSource, UITableViewDelegate, TDStorageResponder>

@property (strong, nonatomic) NSArray *folderSubItems_data;

@property (strong, nonatomic) TDStorageManager *storageManager;

@end

@implementation TDFolderViewController


- (TDStorageManager *)storageManager
{
    if (!_storageManager) {
        _storageManager = [[TDStorageManager alloc] initWithResponder:self];
    }
    return _storageManager;
}

#pragma mark - Init UI Views


- (id)init
{
    self = [super init];
    if (self) {
        
         [self.view addSubview:self.folderSubItems_list];
         [self.view addSubview:self.noData_lbl];
    }
    return self;
}
- (UITableView *)folderSubItems_list
{
    if (!_folderSubItems_list) {
        _folderSubItems_list = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
        _folderSubItems_list.dataSource = self;
        _folderSubItems_list.delegate = self;
        _folderSubItems_list.backgroundColor = [UIColor lightGrayColor];
    }
    return _folderSubItems_list;
}
- (UILabel *)noData_lbl
{
    if (!_noData_lbl) {
        CGRect lbl_frame = CGRectInset(self.view.bounds, 0, 60);
        lbl_frame = CGRectOffset(lbl_frame, 0, -60);
        _noData_lbl = [UILabel labelforFolderWarningWithFrame:lbl_frame];
        _noData_lbl.text = kNoDataString;
        _noData_lbl.hidden = YES;
    }
    return _noData_lbl;

}


#pragma mark - Data Loading


- (void)setFolder:(TDFolder *)folder
{
    _folder = folder;
    self.title = self.folder.name;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self getData];
}
- (void)getData
{
    if (self.folder) {
        //если данные о содержимом  папки уже заргужены, просто выводи их
        if (self.folder.subItems) {
            [self showDataFromCurrentFolder];
        }else
        {
            self.action.hidden = NO;
            [self startLoading];
            [self.storageManager getHierarchy:self.folder];
        }
    }
}
-(void)showDataFromCurrentFolder
{
    //Пусть у контроллера будет своя копия данных, таблица обновлятся из этого массива
    //и если мы паралельно будем вносить изменения будет беда
    self.folderSubItems_data = [self.folder.subItems copy];
    //Проверяем, а вдруг папка пуста
    [self showNoData:(self.folderSubItems_data == 0)];
    [self.folderSubItems_list reloadData];
    [self stopLoading];
}
-(void)showNoData:(BOOL)dataHidden
{
    self.folderSubItems_list.hidden = dataHidden;
    self.noData_lbl.hidden = !dataHidden;
}
-(void)showDataError:(NSError *)error
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:[error localizedDescription]   delegate:nil cancelButtonTitle:@"ОК" otherButtonTitles:nil];
    [alert show];
    [self stopLoading];
}


#pragma mark - UI Storage responder

-(void)storageDidFinishLoadingAction:(id<TDAction>)action
{
    //завершилась обработка текущей папки, можно посмотреть результат
     [self performSelectorOnMainThread:@selector(showDataFromCurrentFolder) withObject:nil waitUntilDone:NO];
}
-(void)storageAction:(id<TDAction>)action FailWithError:(NSError *)error
{
    //случилось что-то плохое, нужно уведомить пользователя
    [self performSelectorOnMainThread:@selector(showDataError:) withObject:error waitUntilDone:NO];
}


- (void)didReceiveMemoryWarning
{
    [self.folder clearAllImages];
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - UITableView Delegates


-(TDItem *)dataFromIndexPath:(NSIndexPath *)indexPath
{
    //to prevent an Exception
    if (self.folderSubItems_data.count>indexPath.row) {
        TDItem *data = [self.folderSubItems_data objectAtIndex:indexPath.row];
        if ([data isKindOfClass:[TDItem class]]) {
            return data;
        }
        NSLog(@"Some WRONG Class in our DATA %@", [data class]);
        return nil;
    }
    return nil;
}
-(id<TDCellsFactory>)currentFactoryForItem:(TDItem *)currentItem
{
    if (currentItem) {
        if ([currentItem isKindOfClass:[TDFolder class]]) {
         return  [[TDFolderCellsFactory alloc] init];
        }else
         return [[TDFileCellsFactory alloc] init];
    }
    return [[TDDefaultCellsFactory alloc] init];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TDItem *currentItem = [self dataFromIndexPath:indexPath];
    id<TDCellsFactory> currentFactory = [self currentFactoryForItem:currentItem];
    return [currentFactory cellHeightForData:currentItem];
}

-(int)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(int)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.folderSubItems_data.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TDItem *currentItem = [self dataFromIndexPath:indexPath];
    id<TDCellsFactory> currentFactory = [self currentFactoryForItem:currentItem];
    return [currentFactory cellForTable:tableView andData:currentItem indexPath:indexPath];
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    TDItem *currentItem = [self dataFromIndexPath:indexPath];
    if ([currentItem isKindOfClass:[TDFolder class]]) {
        TDFolderViewController *next_folder = [[TDFolderViewController alloc] init];
        next_folder.folder = (TDFolder *)currentItem;
        [self.navigationController pushViewController:next_folder animated:YES];
    }
    if ([currentItem isKindOfClass:[TDFile class]]) {
        TDFileViewController *show_file = [[TDFileViewController alloc] init];
        show_file.file =(TDFile *)currentItem;
        [self.navigationController pushViewController:show_file animated:YES];
    }
}

@end
