//
//  TDXMLParser.h
//  SampleRequest
//
//  Created by Olena Babenko on 2/8/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import <Foundation/Foundation.h>

@class TDXMLParser;
@protocol TDXMLParserDelegate <NSObject>

-(void)parser:(TDXMLParser*)parser DidEndDocumentWithResult:(NSArray *)result;
-(void)parser:(TDXMLParser*)parser DidFailed:(NSError *)result;

@end
@interface TDXMLParser : NSObject<NSXMLParserDelegate>



-(void)parseXMLData:(NSData *)data completion:(void(^)(NSArray *result, NSError *error))callback;

-(void)parseXMLData:(NSData *)data withDelegate:(id<TDXMLParserDelegate>)delegate;

@end
