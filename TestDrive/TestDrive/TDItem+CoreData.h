//
//  TDItem+CoreData.h
//  TestDrive
//
//  Created by Olena Babenko on 2/10/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import "TDItem.h"
#import "TDItemMO.h"

@interface TDItem (CoreData)


+(instancetype)itemWithManagedObject:(TDItemMO *)managedObject;

-(void)updateWithManagedObject:(TDItemMO *)managedObject;


-(void)updateWithCurrentItemManagedObject:(TDItemMO *)managedObject;
@end
