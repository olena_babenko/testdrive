//
//  TDItemMO.h
//  TestDrive
//
//  Created by Olena Babenko on 2/10/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface TDItemMO : NSManagedObject

@property (nonatomic, retain) NSNumber * object_id;
@property (nonatomic, retain) NSString * localPath;
@property (nonatomic, retain) NSDate * lastModified;
@property (nonatomic, retain) NSDate * creation;
@property (nonatomic, retain) NSString * contentType;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSNumber * size;

@property (nonatomic, retain) NSString * parent_id;
@property (nonatomic, retain) NSNumber *is_folder;

@end
