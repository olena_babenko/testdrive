//
//  TDItemMO.m
//  TestDrive
//
//  Created by Olena Babenko on 2/10/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import "TDItemMO.h"


@implementation TDItemMO

@dynamic object_id;
@dynamic localPath;
@dynamic lastModified;
@dynamic creation;
@dynamic contentType;
@dynamic name;
@dynamic size;
@dynamic parent_id;
@dynamic is_folder;

@end
