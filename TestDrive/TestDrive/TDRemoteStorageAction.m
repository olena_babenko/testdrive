//
//  TDRemoteStorageAction.m
//  TestDrive
//
//  Created by Olena Babenko on 2/8/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import "TDRemoteStorageAction.h"
#import "TDXMLParser.h"
#import "TDRemoteStorageAction_Privat.h"
#import "NSError+StorageError.h"

@interface TDRemoteStorageAction ()<TDXMLParserDelegate>
{
    NSMutableData *loadedData;
    BOOL responseSuccess;
}


@end

@implementation TDRemoteStorageAction


-(TDItem *)item
{
    return _item;
}

-(void)runItem:(TDItem *)item withResponder:(id<TDStorageResponder>)responder
{
    self.responder = responder;
    self.item = item;
    [self loadRequest];
}
//get all of составляющие запроса individually
-(NSURL *)path
{
    return [NSURL URLWithString:@"https://webdav.yandex.ru:443"];
}
-(NSData *)data
{
    return nil;
}
-(NSString *)method
{
    return @"GET";
}
-(void)addHeadersToRequest:(NSMutableURLRequest *)request
{
    //add AUTH header
    NSString *token = [[NSUserDefaults standardUserDefaults] objectForKey:kTokenKey];
    NSString *auth_sth = [NSString stringWithFormat:@"OAuth %@", token];
    [request setValue:auth_sth forHTTPHeaderField:@"Authorization"];
    //[request setValue:@"OAuth 4446e3dbdf724795bca582a8002f26ee" forHTTPHeaderField:@"Authorization"];
}
-(void)prepeareDataHolder
{
    if (!loadedData) {
        loadedData = [[NSMutableData alloc] init];
    }else
    {
        [loadedData setLength:0];
    }
}
-(void)loadRequest
{
    NSString *token = [[NSUserDefaults standardUserDefaults] objectForKey:kTokenKey];
    if (token) {
        [self prepeareDataHolder];
        
        NSURL *path = [self path];
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:path];
        [request setHTTPMethod:[self method]];
        
        NSData *additionalData = [self data];
        if (additionalData) {
            [request setHTTPBody:additionalData];
        }
        
        [self addHeadersToRequest:request];
        
        [NSURLConnection connectionWithRequest:request delegate:self];
    }
    
}


#pragma mark - Parse Data

-(void)parseData:(NSData *)data
{
    //Parse data only in success case
    TDXMLParser *parser = [[TDXMLParser alloc] init];
    [parser parseXMLData:data withDelegate:self];
    
}
-(id)parseResponse:(id)response
{
     NSLog(@"Parser result %@", [response description]);
    return nil;
}//Only place were OUT object can changed!If we want to use a Lock it should be there. 
-(void)completeRequestWithAnswer:(id)response
{
    //Удалить хранимые даные
    [loadedData setLength:0];
    //сообщить соответсвующему классу, что загрузка завершена
    if (self.responder) {
        [self.responder storageDidFinishLoadingAction:self];
    }
}


#pragma mark - XML parser


-(void)parser:(TDXMLParser *)parser DidEndDocumentWithResult:(NSArray *)result
{
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [queue addOperationWithBlock:^(void)
    {
        id answer;
        if (result.count>0) {
            
            NSDictionary *statusObject =result[0];
            answer = [self parseResponse:statusObject[@"d:multistatus"][@"d:response"]];
        }
        [self completeRequestWithAnswer:answer];
    }];
}
-(void)parser:(TDXMLParser *)parser DidFailed:(NSError *)result
{
    [loadedData setLength:0];
    if (self.responder) {
        [self.responder storageAction:self FailWithError:result];
    }
    
}


#pragma mark - Connection Delegate



-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
  
    [loadedData appendData:data];
    
}

-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    
    NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
    NSLog(@"Response CoMPLETED With Code %d", httpResponse.statusCode);
    if (httpResponse.statusCode == 207||httpResponse.statusCode == 200) {
        responseSuccess = YES;
    }else
    {
        responseSuccess = NO;
        if (self.responder) {
            [self.responder storageAction:self FailWithError:[NSError errorStorageWithCode:httpResponse.statusCode andDescription:@"Connection error"]];
        }
    }
    
}
-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    if (responseSuccess) {
        [self parseData:loadedData];
    }
   
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSLog(@"Fail with error %@", [error description]);
    [loadedData setLength:0];
    if (self.responder) {
        [self.responder storageAction:self FailWithError:error];
    }
}


@end
