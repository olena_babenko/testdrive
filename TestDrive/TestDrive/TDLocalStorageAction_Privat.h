//
//  TDLocalStorageAction_Privat.h
//  TestDrive
//
//  Created by Olena Babenko on 2/10/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import "TDLocalStorageAction.h"

@interface TDLocalStorageAction ()

-(TDItemMO *)getManagedObjectByItem:(TDItem *)item;
-(void)deleteManagedObjectByItem:(TDItem *)item;
-(TDItemMO *)createManageObjectWithItem:(TDItem *)item;



-(NSArray *)getAllSubItemsByItem:(TDItem *)item;
-(void)deleteManagedObjectWithParentItem:(TDItem *)item;
-(TDItemMO *)createManageObjectWithParentItem:(TDItem *)item;

@property(strong, nonatomic) TDItem *item;
@property(nonatomic, weak) id<TDStorageResponder> responder;
@end
