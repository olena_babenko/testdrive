//
//  UILabel+Style.h
//  TestDrive
//
//  Created by Olena Babenko on 2/9/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (Style)

+(UILabel *)labelforCellTitleWithFrame:(CGRect)frame;
+(UILabel *)labelforCellSubtitleWithFrame:(CGRect)frame;
+(UILabel *)labelforFolderWarningWithFrame:(CGRect)frame;
+(UILabel *)labelforFilePropertiesWithFrame:(CGRect)frame;
@end
