//
//  TDRootFolder.m
//  TestDrive
//
//  Created by Olena Babenko on 2/7/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import "TDRootFolder.h"


@implementation TDRootFolder

-(NSString *)name
{
    return @"Диск";
}
-(NSString *)localPath
{
    return @"/";
}
+(TDFolder *)rootFolder
{
    return [[TDRootFolder alloc] init];
}

@end
