//
//  TDXMLParser.m
//  SampleRequest
//
//  Created by Olena Babenko on 2/8/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import "TDXMLParser.h"

@interface TDXMLParser ()
{
    NSMutableArray *dictionaryStack;
    NSMutableString *textInProgress;
}

@property (nonatomic, copy) void (^parseCompletionBlock)(NSArray *result, NSError *error);
@property (nonatomic, weak) id<TDXMLParserDelegate> delegate;

@end
@implementation TDXMLParser

-(void)parseXMLData:(NSData *)data  completion:(void (^)(NSArray *, NSError *))callback
{
    self.parseCompletionBlock = callback;
    NSXMLParser *parser = [[NSXMLParser alloc] initWithData:data];
    parser.delegate = self;
    [parser parse];
}
-(void)parseXMLData:(NSData *)data withDelegate:(id<TDXMLParserDelegate>)delegate
{
    self.delegate = delegate;
    NSXMLParser *parser = [[NSXMLParser alloc] initWithData:data];
    parser.delegate = self;
    [parser parse];
    
}
-(void)parserDidStartDocument:(NSXMLParser *)parser
{
    //NSLog(@"Start parser");
    dictionaryStack = [[NSMutableArray alloc] init];
    [dictionaryStack addObject:[[NSMutableDictionary alloc] init]];
}
-(void)parserDidEndDocument:(NSXMLParser *)parser
{
    //NSLog(@"End parser");
    if (self.parseCompletionBlock) {
        self.parseCompletionBlock(dictionaryStack, nil);
    }
    if (self.delegate) {
        [self.delegate parser:self DidEndDocumentWithResult:dictionaryStack];
    }
}
-(void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    
    // Get the dictionary for the current level in the stack
    NSMutableDictionary *parentDict = [dictionaryStack lastObject];
    // NSLog(@"parent %@", [parentDict description]);
    // Create the child dictionary for the new element, and initilaize it with the attributes
    NSMutableDictionary *childDict = [[NSMutableDictionary alloc] init];
    
    // If there’s already an item for this key, it means we need to create an array
    id existingValue = parentDict[elementName];
    if (existingValue)
    {
        NSMutableArray *array = nil;
        if ([existingValue isKindOfClass:[NSMutableArray class]])
        {
            // The array exists, so use it
            array = (NSMutableArray *) existingValue;
        }
        else
        {
            // Create an array if it doesn’t exist
            array = [[NSMutableArray alloc] init];
            [array addObject:existingValue];
            
            // Replace the child dictionary with an array of children dictionaries
            parentDict[elementName] = array;
        }
        
        // Add the new child dictionary to the array
        [array addObject:childDict];
    }
    else
    {
      //Make dictionary BY DEFAULT
        parentDict[elementName] = childDict;
    }
    
    // Update the stack
    [dictionaryStack addObject:childDict];
}
-(void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    NSString *trimmed = [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    trimmed = [trimmed stringByTrimmingCharactersInSet:[NSCharacterSet newlineCharacterSet]];
    if (![trimmed isEqualToString:@""]) {
        NSMutableString *dicString = [string mutableCopy];
        textInProgress = dicString;
    }
    
}
-(void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    // Update the parent dict with text info
    NSInteger preLastObjIndex = dictionaryStack.count -2;
    if (preLastObjIndex>0) {
        NSMutableDictionary *dictInProgress = [dictionaryStack objectAtIndex:preLastObjIndex];
        
        // Set the text property
        if ([textInProgress length] > 0)
        {
            textInProgress = (NSMutableString *)[textInProgress stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            //Replace last Dictionary with STRING
            dictInProgress[elementName] = textInProgress;
            
            // Reset the text
            textInProgress = [[NSMutableString alloc] init];
        }
        
        // Pop the current dict
        [dictionaryStack removeLastObject];
    }
}
-(void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError
{
    NSLog(@"Paser error %@", parseError.description);
    if (self.parseCompletionBlock) {
        self.parseCompletionBlock(nil, parseError);
    }
    
    if (self.delegate) {
        [self.delegate parser:self DidFailed:parseError];
    }
}

@end
