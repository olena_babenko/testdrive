//
//  TDLocalStorageAction.m
//  TestDrive
//
//  Created by Olena Babenko on 2/8/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import "TDLocalStorageAction.h"
#import "TDItem+CoreData.h"
#import "TDLocalStorageAction_Privat.h"

#define kTDItemEntityName @"TDItemMO"

@implementation TDLocalStorageAction

-(TDItem *)item
{
    return _item;
}

-(void)runItem:(TDItem *)item withResponder:(id<TDStorageResponder>)responder
{
    self.item = item;
    self.responder = responder;
    
}

-(TDItemMO *)getManagedObjectByItem:(TDItem *)item
{
    //Unique object identifier - his path
    return (TDItemMO *)[[TDCoreDB_Imp sharedInstance] getObjectByField:@"localPath" value:item.localPath entity:kTDItemEntityName];
}
-(void)deleteManagedObjectByItem:(TDItem *)item
{
    //Unique object identifier - his path
    [[TDCoreDB_Imp sharedInstance] deleteObjectByField:@"localPath" value:item.localPath entity:kTDItemEntityName];
}
-(TDItemMO *)createManageObjectWithItem:(TDItem *)item
{
    //Unique object identifier - his path
    TDItemMO *createdItem = (TDItemMO *)[[TDCoreDB_Imp sharedInstance] createManagedObjectInEntity:kTDItemEntityName];
    [item updateWithCurrentItemManagedObject:createdItem];
    return createdItem;
}




-(NSArray *)getAllSubItemsByItem:(TDItem *)item
{
    //Unique object identifier - his path
    return [[TDCoreDB_Imp sharedInstance] getAllObjectByField:@"parent_id" value:item.localPath entity:kTDItemEntityName];
}
-(void)deleteManagedObjectWithParentItem:(TDItem *)item
{
    //Unique object identifier - his path
    [[TDCoreDB_Imp sharedInstance] deleteAllObjectByField:@"parent_id" value:item.localPath entity:kTDItemEntityName];
}
-(TDItemMO *)createManageObjectWithParentItem:(TDItem *)item
{
    //Unique object identifier - his path
    TDItemMO *createdItem = (TDItemMO *)[[TDCoreDB_Imp sharedInstance] createManagedObjectInEntity:kTDItemEntityName];
    createdItem.parent_id = item.localPath;
    return createdItem;
}
    
@end
