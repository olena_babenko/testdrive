//
//  TDCellsFactory.h
//  TestDrive
//
//  Created by Olena Babenko on 2/8/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import <Foundation/Foundation.h>

//Тип для того чтобы строить одинаковые селы в разных контроллерах
//и показывать в одном контроллере много разных по типу селов

//Может быть и не нужная сложность для простого такого простого проэкта, но если захочеться сделать различные селы для раных типов файлов, это может пригодиться

@protocol TDCellsFactory <NSObject>


-(UITableViewCell *)cellForTable:(UITableView *)table andData:(id)data indexPath:(NSIndexPath *)indexPath;
-(CGFloat)cellHeightForData:(id)data;

@end
