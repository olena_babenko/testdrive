//
//  TDLoginViewController.h
//  TestDrive
//
//  Created by Olena Babenko on 2/8/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TDAuthViewController.h"

@interface TDLoginViewController : TDAuthViewController

@end
