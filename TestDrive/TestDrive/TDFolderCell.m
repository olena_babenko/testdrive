//
//  TDFolderCell.m
//  TestDrive
//
//  Created by Olena Babenko on 2/8/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import "TDFolderCell.h"
#import "TDFolder.h"

@implementation TDFolderCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
         self.contentView.backgroundColor = [UIColor colorWithRed:25/255.0 green:45/255.0  blue:85/255.0  alpha:0.3];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setData:(TDItem *)data
{
    if ([data isKindOfClass:[TDFolder class]]) {
        [super setData:data];
        self.icon_lbl.image = [UIImage imageNamed:@"folder.png"];
    }
}


@end
