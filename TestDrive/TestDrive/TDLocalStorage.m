//
//  TDLocalStorage.m
//  TestDrive
//
//  Created by Olena Babenko on 2/9/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import "TDLocalStorage.h"
#import "TDUpdateLocal.h"
#import "TDGetHierarchyLocal.h"
#import "TDExistanseLocal.h"

#import "TDCoreDB_Imp.h"

@implementation TDLocalStorage

-(void)getHierarchy:(TDItem *)item withResponder:(id<TDStorageResponder>)responder
{
    //get current folder info from DB
    TDGetHierarchyLocal *hierarchy  = [[TDGetHierarchyLocal alloc] init];
    [hierarchy runItem:item withResponder:responder];
}
-(void)update:(TDItem *)item withResponder:(id<TDStorageResponder>)responder
{
    //Update Local Storage With Data
    TDUpdateLocal *update = [[TDUpdateLocal alloc] init];
    [update runItem:item withResponder:responder];
    
}
-(BOOL)isExist:(TDItem *)item
{
    TDExistanseLocal *existanse = [[TDExistanseLocal alloc] init];
    return [existanse checkItem:item];
}
-(void)getBigPreivew:(TDItem *)item withResponder:(id<TDStorageResponder>)responder
{
    
}

@end
