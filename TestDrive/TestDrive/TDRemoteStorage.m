//
//  TDRemoteStorage.m
//  TestDrive
//
//  Created by Olena Babenko on 2/9/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import "TDRemoteStorage.h"
#import "TDGetHierarchyRemote.h"
#import "TDPicturePreviewRemote.h"

@implementation TDRemoteStorage

-(void)getHierarchy:(TDItem *)item withResponder:(id<TDStorageResponder>)responder
{
    TDGetHierarchyRemote *hierarchy = [[TDGetHierarchyRemote alloc] init];
    [hierarchy runItem:item withResponder:responder];
}
-(void)update:(TDItem *)item withResponder:(id<TDStorageResponder>)responder
{
    //for updating folder structure if we would change some file/folder properties locally
}
-(BOOL)isExist:(TDItem *)item
{
    //method for checking if file still exist on remote storage
    return YES;
}
-(void)getBigPreivew:(TDItem *)item withResponder:(id<TDStorageResponder>)responder
{
    TDPicturePreviewRemote *big_preview = [[TDPicturePreviewRemote alloc] init];
    [big_preview runItem:item withResponder:responder];
}
@end
