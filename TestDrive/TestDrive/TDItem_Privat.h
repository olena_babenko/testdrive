//
//  TDItem_Privat.h
//  TestDrive
//
//  Created by Olena Babenko on 2/10/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import "TDItem.h"

@interface TDItem ()

@property(nonatomic, readwrite, strong) NSString *name;
@property(nonatomic, readwrite, strong) NSString *localPath;
@property(nonatomic, readwrite, strong) NSURL *publicURL;
@property(nonatomic, readwrite, strong) NSDate *lastModified;
@property(nonatomic, readwrite, strong) NSDate *created;
@property(nonatomic, readwrite, strong) NSString *contentType;

@property(nonatomic, readwrite, assign) long long size;
@property(nonatomic, readwrite, assign) BOOL isReadOnly;
@property(nonatomic, readwrite, assign) BOOL isloadSuccesfully;

@end
