//
//  TDFileCellsFactory.h
//  TestDrive
//
//  Created by Olena Babenko on 2/8/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TDCellsFactory.h"

@interface TDFileCellsFactory : NSObject<TDCellsFactory>

@end
