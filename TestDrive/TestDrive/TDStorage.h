//
//  TDStorage.h
//  TestDrive
//
//  Created by Olena Babenko on 2/7/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TDItem.h"
#import "TDStorageResponder.h"

@protocol TDStorage <NSObject>

-(void)getHierarchy:(TDItem *)item withResponder:(id<TDStorageResponder>)responder;
-(void)update:(TDItem *)item withResponder:(id<TDStorageResponder>)responder;
-(void)getBigPreivew:(TDItem *)item withResponder:(id<TDStorageResponder>)responder;

-(BOOL)isExist:(TDItem *)item;

@end
