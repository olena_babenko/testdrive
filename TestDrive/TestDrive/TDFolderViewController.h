//
//  TDFolderViewController.h
//  TestDrive
//
//  Created by Olena Babenko on 2/7/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TDFolder.h"
#import "TDViewController.h"

@interface TDFolderViewController : TDViewController


@property (strong, nonatomic) UITableView *folderSubItems_list;
@property (strong, nonatomic) UILabel *noData_lbl;


@property (strong, nonatomic) TDFolder *folder;

@end
