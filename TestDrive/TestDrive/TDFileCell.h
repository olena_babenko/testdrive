//
//  TDFileCell.h
//  TestDrive
//
//  Created by Olena Babenko on 2/8/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TDFile.h"
#import "TDItemCell.h"

@interface TDFileCell : TDItemCell


@property (strong, nonatomic) UILabel *size_lbl;


@end
