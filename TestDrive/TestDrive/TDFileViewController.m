//
//  TDFileViewController.m
//  TestDrive
//
//  Created by Olena Babenko on 2/9/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import "TDFileViewController.h"
#import "UILabel+Style.h"

#import "TDStorageManager.h"

@interface TDFileViewController ()<TDStorageResponder>

@property (strong, nonatomic) TDStorageManager *storageManager;

@end

@implementation TDFileViewController


- (id)init
{
    self = [super init];
    if (self) {
        [self.view addSubview:self.file_img];
        [self.view addSubview:self.size_lbl];
        [self.view addSubview:self.type_lbl];
        [self.view addSubview:self.created_lbl];
        [self.view addSubview:self.modified_lbl];
        
    }
    return self;
}
- (TDStorageManager *)storageManager
{
    if (!_storageManager) {
        _storageManager = [[TDStorageManager alloc] initWithResponder:self];
    }
    return _storageManager;
}
- (UIImageView *)file_img
{
    if (!_file_img) {
        CGRect file_frame = CGRectMake(0, 0, self.view.frame.size.width, 150);
        _file_img = [[UIImageView alloc] initWithFrame:file_frame];
       [_file_img setContentMode:UIViewContentModeScaleAspectFill];
        [_file_img setClipsToBounds:YES];
        
    }
    return _file_img;
}
- (UILabel *)size_lbl
{
    if (!_size_lbl) {
        
        CGRect align = self.file_img.frame;
        CGFloat dx = 10;
        CGFloat dy = 5;
        CGRect size_frame = CGRectMake(dx, CGRectGetMaxY(align)+dy, align.size.width - 2*dx, 30);
        _size_lbl = [UILabel labelforFilePropertiesWithFrame:size_frame];
    }
    return _size_lbl;
}
- (UILabel *)type_lbl
{
    if (!_type_lbl) {
        CGRect align = self.size_lbl.frame;
        CGRect type_frame = align;
        _type_lbl = [UILabel labelforFilePropertiesWithFrame:type_frame];
        _type_lbl.textAlignment = NSTextAlignmentRight;
    }
    return _type_lbl;
}
- (UILabel *)created_lbl
{
    if (!_created_lbl) {
        CGRect align = self.size_lbl.frame;
        //Make offset a bit Y
        CGRect created_frame = CGRectOffset(align, 0, align.size.height +5);
        _created_lbl = [UILabel labelforFilePropertiesWithFrame:created_frame];
    }
    return _created_lbl;
    
}
- (UILabel *)modified_lbl
{
    if (!_modified_lbl) {
        CGRect align = self.created_lbl.frame;
        CGRect modified_frame = CGRectOffset(align, 0, align.size.height + 5);
        _modified_lbl = [UILabel labelforFilePropertiesWithFrame:modified_frame];
    }
    return _modified_lbl;
}

- (void)setFile:(TDFile *)file
{
    _file = file;
    
    _size_lbl.text = [file sizeStringAsIs];
    _type_lbl.text = file.contentType;
    if (!file.contentType) {
        _type_lbl.hidden = YES;
    }
    
    [self setAnPreviewImage];
    
    _created_lbl.text = [NSString stringWithFormat:@"Creation date: %@",[file createdString]];
    _modified_lbl.text =[NSString stringWithFormat:@"Last modified: %@",[file lastMofifiedString]];

    
}
-(void)setAnPreviewImage
{
    if (self.file.file_img) {
        self.file_img.image = self.file.file_img;
    }else
    {
        if ([self.file isImageContent]) {
            NSLog(@"Got an Image content");
            [self.storageManager getBigImagePreview:self.file];
        }
    }
}
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    self.file.file_img = nil;
    //can be restored
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UI Storage responder

-(void)storageDidFinishLoadingAction:(id<TDAction>)action
{
    //завершилась обработка текущей папки, можно посмотреть результат
    [self performSelectorOnMainThread:@selector(setAnPreviewImage) withObject:nil waitUntilDone:NO];
}
-(void)storageAction:(id<TDAction>)action FailWithError:(NSError *)error
{
    //случилось что-то плохое
}
@end
