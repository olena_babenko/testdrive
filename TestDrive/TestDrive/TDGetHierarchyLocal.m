//
//  TDGetHierarchyLocalAction.m
//  TestDrive
//
//  Created by Olena Babenko on 2/8/14.
//  Copyright (c) 2014 Olena Babenko. All rights reserved.
//

#import "TDGetHierarchyLocal.h"
#import "TDLocalStorageAction_Privat.h"
#import "TDItem+CoreData.h"
#import "TDRootFolder.h"

@implementation TDGetHierarchyLocal

-(void)runItem:(TDItem *)item withResponder:(id<TDStorageResponder>)responder
{
    //we dont change current object
    //just ject next hierarchy
    if ([item isKindOfClass:[TDFolder class]]){
        [self getSubitemsFromParent:(TDFolder*)item];
    }
    if (responder) {
        [responder storageDidFinishLoadingAction:self];
    }
}

-(void)getSubitemsFromParent:(TDFolder *)item
{
    //GetAllSubItems as MO
    NSArray *subItemsMO = [self getAllSubItemsByItem:item];
    //[item removeItems];
    NSMutableArray *subItems = [[NSMutableArray alloc] init];
    for (TDItemMO *subItem in subItemsMO) {
            
        //convert every to TDItem
        TDItem *storedItem = [TDItem itemWithManagedObject:subItem];
        //addTo Item and Fount Possible SubItems
        NSLog(@"Adding item with name %@", storedItem.name);
        //[item addItem:storedItem];
        [subItems addObject:storedItem];
        if ([storedItem isKindOfClass:[TDFolder class]]) {
            [self getSubitemsFromParent:(TDFolder *)storedItem];
        }
    }
    
    [item replaceItems:subItems];
    
}
@end
